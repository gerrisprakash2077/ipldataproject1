
const path=require('path')
const numberOfMatchesPlayedPerYear=require(path.resolve(__dirname,'./1-matches-per-year'));
//console.log(numberOfMatchesPlayedPerYear())
test("1-matches-per-year",()=>{
    expect(numberOfMatchesPlayedPerYear()).toStrictEqual({
        "2008": 58,
        "2009": 57,
        "2010": 60,
        "2011": 73,
        "2012": 74,
        "2013": 76,
        "2014": 60,
        "2015": 59,
        "2016": 60,
        "2017": 59
    })
})