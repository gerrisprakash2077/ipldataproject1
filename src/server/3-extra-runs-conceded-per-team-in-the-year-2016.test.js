
const path=require('path')
const numberOfMatchesPlayedPerYear=require(path.resolve(__dirname,'./3-extra-runs-conceded-per-team-in-the-year-2016'));
//console.log(numberOfMatchesPlayedPerYear())
test("./3-extra-runs-conceded-per-team-in-the-year-2016",()=>{
    expect(numberOfMatchesPlayedPerYear()).toStrictEqual({
        "Rising Pune Supergiants": 108,
        "Mumbai Indians": 102,
        "Kolkata Knight Riders": 122,
        "Delhi Daredevils": 106,
        "Gujarat Lions": 98,
        "Kings XI Punjab": 100,
        "Sunrisers Hyderabad": 107,
        "Royal Challengers Bangalore": 156
      })
})