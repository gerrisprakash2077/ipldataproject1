//Find the strike rate of a batsman for each season
const fs=require('fs');
const path=require('path');
function strikeRate(batsman){
    const matchesObjectArray=require('./matchesCsvToJsObject');
    const deliveriesObjectArray=require('./deliveriesCsvToJsObject');

    let maxSeason=Number(matchesObjectArray[0].season);
    let minSeason=Number(matchesObjectArray[0].season);

    // logic to find starting season and endong season

    matchesObjectArray.forEach((elements)=>{
        if(Number(elements.season)<minSeason){
            minSeason=Number(elements.season);
        }
        if(Number(elements.season)>maxSeason){
            maxSeason=Number(elements.season);
        }
    })



    let strikeRateOfBatsman={};

    //iterate over all the season from 2008 to 2017
    for(let season=minSeason;season<=maxSeason;season++){

        // seasonMatchId will contain the all match id's of the current particular season 
        let seasonMatchId=matchesObjectArray.filter((items)=>{
            return Number(items.season)==season;
        }).map((item)=>{
            return item.id;
        });

        //console.log(seasonMatchId);
        
        
        let totalRuns=0;
        let totalBalls=0;
        /*itterates over the deliveries object from csv and checks weather the match is part of the season
        and check if the batsman is the batsman passsed from the main function and gets the total runs and total balls 
        for the particular batsman for the particular season
        */

        //filtering with season
        deliveriesObjectArray.filter((items)=>{
            // to check weather the id is part of the particular season
            return seasonMatchId.find((elements)=>{
                return elements==items.match_id;
            })

            //filtering with batsman name
        }).filter((items)=>{
            return items.batsman==batsman

            //counting runs and balls
        }).forEach((items)=>{
            totalRuns+=Number(items.batsman_runs);
            totalBalls+=1;
        })

        strikeRateOfBatsman[season]=(totalRuns/totalBalls)*100;
    }
    return strikeRateOfBatsman;
}

//console.log(strikeRate("V Kohli"));
module.exports=strikeRate;
//writing result into output json file
const jsonFormat=JSON.stringify(strikeRate("MS Dhoni"),null,2);
fs.writeFile(path.resolve(__dirname,'../public/output/7-strike-rate-of-a-batsman-each-season.json'),jsonFormat,(err)=>{});