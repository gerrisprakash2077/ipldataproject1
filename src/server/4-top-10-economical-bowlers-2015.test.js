
const path=require('path')
const top10EconomicalPlayerFunction=require(path.resolve(__dirname,'./4-top-10-economical-bowlers-2015'));
//console.log(numberOfMatchesPlayedPerYear())
test("4-top-10-economical-bowlers-2015",()=>{
    expect(top10EconomicalPlayerFunction()).toStrictEqual([
        "RN ten Doeschate",
        "J Yadav",
        "V Kohli",
        "R Ashwin",
        "S Nadeem",
        "Z Khan",
        "Parvez Rasool",
        "MC Henriques",
        "MA Starc",
        "M de Lange"
      ])
})