//Find a player who has won the highest number of Player of the Match awards for each season
const fs=require('fs');
const path=require('path')
function maxMvpFunction(){
    const matchesObjectArray=require('./matchesCsvToJsObject');
    //logic to find min amd max season
    let maxSeason=Number(matchesObjectArray[0].season);
    let minSeason=Number(matchesObjectArray[0].season);
    matchesObjectArray.forEach((elements)=>{
        if(Number(elements.season)<minSeason){
            minSeason=Number(elements.season);
        }
        if(Number(elements.season)>maxSeason){
            maxSeason=Number(elements.season);
        }
    })
    //iterating from min season to max season
    let maximumMVPPlayer={};
    for(let i=minSeason;i<=maxSeason;i++){
        // filtering out the matches happened in the current season and storing in season data
        let seasonData=matchesObjectArray.filter((value)=>{
            return Number(value.season)===i;
        })

        //console.log(seasonData);
        //season mvp count contains the player name as key with the number of mvp that player won in the current season
        //iterating over season data and incremeting the count of mvp won by particular player
        let seasonMvpCount=seasonData.reduce((ac,cv)=>{
            if(ac[cv.player_of_match]){
                ac[cv.player_of_match]++;
            }
            else{
                ac[cv.player_of_match]=1;
            }
            return ac;
        },{});

        // logic to find the player who has maximum mvp
        let maxMvpNo=-1;
        let mvp="";
        Object.keys(seasonMvpCount).forEach((j)=>{
            if(seasonMvpCount[j]>maxMvpNo){
                maxMvpNo=seasonMvpCount[j];
                mvp=j;
            }
        });
        
        maximumMVPPlayer[i]=mvp;
        //console.log(i,mvp);
    }
    //console.log(result);
    return maximumMVPPlayer;
}
module.exports=maxMvpFunction;
//writing result into output json file
const jsonFormat=JSON.stringify(maxMvpFunction(),null,2);
fs.writeFile(path.resolve('../public/output/6-player-won-highest-mvp-each-season.json'),jsonFormat,()=>{});