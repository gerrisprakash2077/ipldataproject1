const fs = require("fs");
const path=require('path')

csv = fs.readFileSync(path.resolve(__dirname,"../data/deliveries.csv")) 
let csvString = csv.toString().split("\r\n");


let keyString=csvString[0];
let keyArray=keyString.split(',')

let deliveriesObjectArray=[]
for(let index=1;index<csvString.length;index++){
    let obj={};
    let valuesArray=csvString[index].split(',');
    //console.log(valuesArray)
    for(let j=0;j<keyArray.length;j++){
        obj[keyArray[j]]=valuesArray[j];
    }
    deliveriesObjectArray.push(obj);
}


module.exports=deliveriesObjectArray;
//console.log(deliveriesObjectArray);