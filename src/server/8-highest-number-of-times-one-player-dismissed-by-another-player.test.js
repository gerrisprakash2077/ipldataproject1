
const path=require('path')
const dPlayerFunction=require(path.resolve(__dirname,'./8-highest-number-of-times-one-player-dismissed-by-another-player'));
//console.log(numberOfMatchesPlayedPerYear())
test("8-highest-number-of-times-one-player-dismissed-by-another-player",()=>{
    expect(dPlayerFunction()).toStrictEqual({
        "batsman": "MS Dhoni",
        "bowler": "Z Khan",
        "dismissCount": 7
      })
})