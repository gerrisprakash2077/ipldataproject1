//Number of matches playe\d per year for all the years in IPL.
const fs = require("fs");
const path=require('path');

function numberOfMatchesPlayedPerYear(){
    const matchesObjectArray=require("./matchesCsvToJsObject.js")
    
    let matchesPeryear=matchesObjectArray.reduce((ac,cv)=>{
        if(ac[cv.season]){
            ac[cv.season]++;
        }
        else{
            ac[cv.season]=1;
        }
        return ac;
    },{});
    
    delete matchesPeryear["undefined"]
    return matchesPeryear;
}

//console.log(numberOfMatchesPlayedPerYear())

module.exports=numberOfMatchesPlayedPerYear;
const jsonFormat=JSON.stringify(numberOfMatchesPlayedPerYear(),null,2);
fs.writeFile(path.resolve(__dirname,'../public/output/1-matches-per-year.json'),jsonFormat,()=>{});

