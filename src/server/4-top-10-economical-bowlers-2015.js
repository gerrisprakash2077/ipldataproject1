//Top 10 economical bowlers in the year 2015
const fs=require('fs');
function top10EconomicalPlayerFunction(){
    const deliveriesObjectArray=require('./deliveriesCsvToJsObject');
    const matchesObjectArray=require('./matchesCsvToJsObject')
    //to extract the matches played in 2015
    let matchId2015=matchesObjectArray.filter((items)=>{
        return items.season=='2015';
    }).map((items)=>{
        return items.id;
    });
    //console.log(matchId2015);

    //contains bowler with runs given and count of balls
    let bowlerRunsWithOver=deliveriesObjectArray.filter((item)=>{
        return matchId2015.find((elements)=>{
            return elements==item.match_id;
        })
    }).reduce((ac,cv)=>{
        if(ac[cv.bowler]){
            ac[cv.bowler].runs+=Number(cv.total_runs);
            ac[cv.bowler].balls+=1;
        }
        else{
            ac[cv.bowler]={}
            ac[cv.bowler].runs=Number(cv.total_runs);
            ac[cv.bowler].balls=1;
        }
        return ac;
    },{})


    //coverts bowler with balls and runs to bowler with economy
    let bowlerWithEconomy={};
    Object.keys(bowlerRunsWithOver).forEach((i)=>{
        bowlerWithEconomy[i]=Number(bowlerRunsWithOver[i].runs)/Number(bowlerRunsWithOver[i].balls);
    })
    //console.log(bowlerWithEconomy);
    //contains top 10 economical bowler
    let top10EconomicalPlayer=[];
    //finds the bowler with minimum economy
    function findMinEconomyBowler(economy){
        let min=99999;
        let result="";
        Object.keys(economy).forEach((i)=>{
            if(economy[i]<min){
                min=economy[i]
                result=i;
            }
        });
        return result;
    }
    // loop runs 10 times so that 10 time a minimum economical player can be found and removed 
    //from the original array
    let index=0;
    while(index++<10){
        let result=findMinEconomyBowler(bowlerWithEconomy) ;
        top10EconomicalPlayer.push(result);
        delete bowlerWithEconomy[result];
    }
    //console.log(top10EconomicalPlayer)
    return top10EconomicalPlayer;
}
module.exports=top10EconomicalPlayerFunction;
const jsonFormat=JSON.stringify(top10EconomicalPlayerFunction(),null,2);
fs.writeFile('../public/output/4-top-10-economical-bowler-in-2015.json',jsonFormat,()=>{});


