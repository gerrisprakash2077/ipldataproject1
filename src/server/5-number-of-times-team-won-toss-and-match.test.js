
const path=require('path')
const wonTossAndGameFunction=require(path.resolve(__dirname,'./5-number-of-times-team-won-toss-and-match'));
//console.log(numberOfMatchesPlayedPerYear())
test("5-number-of-times-team-won-toss-and-match",()=>{
    expect(wonTossAndGameFunction()).toStrictEqual({
        "Rising Pune Supergiant": 5,
        "Kolkata Knight Riders": 44,
        "Kings XI Punjab": 28,
        "Royal Challengers Bangalore": 35,
        "Sunrisers Hyderabad": 17,
        "Mumbai Indians": 48,
        "Gujarat Lions": 10,
        "Delhi Daredevils": 33,
        "Chennai Super Kings": 42,
        "Rajasthan Royals": 34,
        "Deccan Chargers": 19,
        "Kochi Tuskers Kerala": 4,
        "Pune Warriors": 3,
        "Rising Pune Supergiants": 3
      })
})