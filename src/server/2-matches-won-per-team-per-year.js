//Number of matches won per team per year in IPL.
const fs=require('fs')
const path=require('path');

function matchesWonPerTeamPerYearFunction(){
    //logic to extract number of matches won per team per year

    const matchesObjectArray=require('./matchesCsvToJsObject.js')

    // contains key as team name and value as another object with keys as years and values as no of matches won
    //iterates over matchesObjectArray check if it hsas the key winner team name 
    //checks if matchesWonPerTeamPerYear has a key on team and adds an object with season as key and count of wins 
    //in that particular season
    let matchesWonPerTeamPerYear=matchesObjectArray.reduce((ac,cv)=>{
        if(ac[cv["winner"]]){
            if(ac[cv.winner][cv.season]>=1){
                ac[cv.winner][cv.season]++;
            }
            else{
                ac[cv.winner][cv.season]=1;
            }
        }
        else{
            ac[cv.winner]={};
            ac[cv.winner][cv.season]=1;
        }
        return ac;
    },{})
    delete matchesWonPerTeamPerYear["undefined"];
    return matchesWonPerTeamPerYear;
    console.log(matchesWonPerTeamPerYear);
}
module.exports=matchesWonPerTeamPerYearFunction;
//writing result into output json file
const jsonFormat=JSON.stringify(matchesWonPerTeamPerYearFunction(),null,2);
fs.writeFile(path.resolve(__dirname,'../public/output/2-matches-won-per-team-per-year.json'),jsonFormat,()=>{});




