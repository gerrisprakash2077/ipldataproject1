//Find the bowler with the best economy in super overs
const fs=require('fs');
const path=require('path');
function bestSuperOverBowlerFunction(){
    const deliveriesObjectArray=require('./deliveriesCsvToJsObject');
    //bowler economy will contain bowler name as key with an ibject with runs and balls
    // iterate over deliveries object array filters super over if yes increment the ball count and runs to the
    // object of bowler as the key
    let bowlerEconomy=deliveriesObjectArray.filter((element)=>{
        return element.is_super_over=='1';
    }).reduce((ac,cv)=>{
        if(ac[cv.bowler]){
            ac[cv.bowler]["runs"]+=Number(cv.total_runs);
            ac[cv.bowler]["balls"]+=1;
        }
        else{
            ac[cv.bowler]={};
            ac[cv.bowler]["runs"]=Number(cv.total_runs);
            ac[cv.bowler]["balls"]=1;
        }
        return ac;
    },{});
    // iterate over deliveries object array check if its a super over if yes increment the ball count and runs to the
    // object of bowler as the key


    //converts the bowler with an object runs,balls to bowler as key and economy as value
    Object.keys(bowlerEconomy).forEach((i)=>{
        bowlerEconomy[i]=Number(bowlerEconomy[i]["runs"]/bowlerEconomy[i]["balls"]);
    });
    
    //console.log(bowlerEconomy);
    //finds the bowler with minimum economy
    let bestSuperOverBowler="";
    let economy=100;

    Object.keys(bowlerEconomy).forEach((i)=>{
        if(bowlerEconomy[i]<economy){
            economy=bowlerEconomy[i];
            bestSuperOverBowler=i;
        }
    });
    //console.log(bestSuperOverBowler);
    return bestSuperOverBowler;
}
module.exports=bestSuperOverBowlerFunction;
//writing result into output json file
const jsonFormat=JSON.stringify(bestSuperOverBowlerFunction());
fs.writeFile(path.resolve(__dirname,'../public/output/9-best-super-over-bowler.json'),jsonFormat,(err)=>{});
