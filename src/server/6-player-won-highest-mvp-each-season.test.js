
const path=require('path')
const maxMvpFunction=require(path.resolve(__dirname,'./6-player-won-highest-mvp-each-season'));
//console.log(numberOfMatchesPlayedPerYear())
test("6-player-won-highest-mvp-each-season",()=>{
    expect(maxMvpFunction()).toStrictEqual({
        "2008": "SE Marsh",
        "2009": "YK Pathan",
        "2010": "SR Tendulkar",
        "2011": "CH Gayle",
        "2012": "CH Gayle",
        "2013": "MEK Hussey",
        "2014": "GJ Maxwell",
        "2015": "DA Warner",
        "2016": "V Kohli",
        "2017": "BA Stokes"
      })
})