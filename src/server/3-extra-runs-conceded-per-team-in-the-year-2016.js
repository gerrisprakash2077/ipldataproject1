//Extra runs conceded per team in the year 2016
const fs=require('fs');
const path=require('path');
function extraRunsCoveredFunction(){
    const deliveriesObjectArray=require('./deliveriesCsvToJsObject')
    const matchesObjectArray=require('./matchesCsvToJsObject')

    // to extract theid of  matches played in 2016
    let matchId2016=matchesObjectArray.filter((items)=>{
        return items.season=='2016';
    }).map((items)=>{
        return items.id;
    });

    //console.log(matchId2016)
    
    // contains team with extra runs given
    //filters if id part of 2016 by finding from the above array
    //reduce adds the extra runs for the particular team
    let extraRunsCovered=deliveriesObjectArray.filter((item)=>{
        return matchId2016.find((element)=>{
            return element==item.match_id;
        })
    }).reduce((ac,cv)=>{
        if(ac[cv.bowling_team]){
            ac[cv.bowling_team]+=Number(cv.extra_runs);
        }
        else{
            ac[cv.bowling_team]=Number(cv.extra_runs);
        }
        return ac;
    },{});
    
    return extraRunsCovered;
//console.log(extraRunsCovered);
}
module.exports=extraRunsCoveredFunction;
//writing result into output json file

const jsonFormat=JSON.stringify(extraRunsCoveredFunction(),null,2);
fs.writeFile(path.resolve('../public/output/3-extra-runs-conceded-per-team-in-the-year-2016.json'),jsonFormat,()=>{});