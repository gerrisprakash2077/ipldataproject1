
const path=require('path')
const strikeRate=require(path.resolve(__dirname,'./7-strike-rate-batsman-each-season'));
//console.log(numberOfMatchesPlayedPerYear())
test("7-strike-rate-batsman-each-season",()=>{
    expect(strikeRate("MS Dhoni")).toStrictEqual({
        "2008": 129.7805642633229,
        "2009": 122.96296296296296,
        "2010": 133.48837209302326,
        "2011": 158.70445344129556,
        "2012": 126.14840989399295,
        "2013": 154.6979865771812,
        "2014": 143.7984496124031,
        "2015": 120.3883495145631,
        "2016": 129.0909090909091,
        "2017": 111.11111111111111
      })
})