//Find the highest number of times one player has been dismissed by another player
const fs=require("fs");
const path=require('path');
function dPlayerFunction(){
    const deliveriesObjectArray=require('./deliveriesCsvToJsObject')
    //playerDismissCount will contain the player name as key with the number of times he got dismissed
    //iterating over deliveries object array if aplayer gets dismissed increment the dismisscount for that particular 
    //player key of the object playerdismisscount.
    let playerDismissCount=deliveriesObjectArray.filter((items)=>{
        return items.player_dismissed;
    }).reduce((ac,cv)=>{
        if(ac[cv.player_dismissed+","+cv.bowler]){
            ac[cv.player_dismissed+","+cv.bowler]++;
        }
        else{
            ac[cv.player_dismissed+","+cv.bowler]=1;
        }
        return ac;
    },{});


    //console.log(playerDismissCount)
    // to find the payer who gets dismissed maximum times.
    let dPlayer=""
    let max=-1;

    Object.keys(playerDismissCount).forEach((i)=>{
        if(playerDismissCount[i]>max){
            max=playerDismissCount[i];
            dPlayer=i;
        } 
    });
    //console.log(dPlayer);
    let playerWithNumberOfTimes={};
    let vengence=dPlayer.split(',');
    playerWithNumberOfTimes.batsman=vengence[0];
    playerWithNumberOfTimes.bowler=vengence[1];
    playerWithNumberOfTimes.dismissCount=max;
    return playerWithNumberOfTimes;
}
module.exports=dPlayerFunction;
//writing result into output json file
const jsonFormat=JSON.stringify(dPlayerFunction(),null,2);
fs.writeFile(path.resolve(__dirname,'../public/output/8-highest-number-of-times-a-player-gets-dismissed-by-another-player.json'),jsonFormat,(err)=>{});


