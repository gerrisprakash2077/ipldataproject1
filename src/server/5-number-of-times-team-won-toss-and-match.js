//Find the number of times each team won the toss and also won the match
const fs=require('fs');
const path=require('path');
function wonTossAndGameFunction(){
    const matchesObjectArray=require('./matchesCsvToJsObject')

    // wonToss and game will contain the team name as key and value representing the 
    //count of the team won the match as well as toss
    //iterating over matches object array checking if tosswinner is the winner of the game. if yes increment the value 
    //for that particular team.
    let wonTossAndGame=matchesObjectArray.filter((items)=>{
        return items.toss_winner===items.winner;
    }).reduce((ac,cv)=>{
        if(ac[cv.winner]){
            ac[cv.winner]++;
        }
        else{
            ac[cv.winner]=1;
        }
        return ac;
    },{});
    

    delete wonTossAndGame["undefined"];
    return wonTossAndGame;
//console.log(wonTossAndGame);
}
module.exports=wonTossAndGameFunction;
//writing result into output json file
const jsonFormat=JSON.stringify(wonTossAndGameFunction(),null,2);
fs.writeFile(path.resolve(__dirname,'../public/output/5-number-of-times-team-won-toss-and-game.json'),jsonFormat,(err)=>{});